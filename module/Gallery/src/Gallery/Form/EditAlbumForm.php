<?php
/**
 * Created by PhpStorm.
 * User: Долгов
 * Date: 04.12.2015
 * Time: 11:15
 */

namespace Gallery\Form;

Use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter;

class EditAlbumForm extends Form{
    public function __construct($name = null){
        parent::__construct('album');
        $this->addElements();
        $this->addInputFilter();
    }

    public function addElements(){
        $this->add(array(
            'name' => 'id',
            'type' => "Hidden",
        ));

        $this->add(array(
            'name' => 'header',
            'type' => 'Text',
            'options' => array('label' => 'Title',),
        ));

        $file = new Element\File('image-file');
        $file->setLabel('Album image upload')->setAttribute('imageFile', 'image-file');
        $this->add($file);


        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submittbutton',
            ),
        ));
    }

    public function addInputFilter(){
        $inputFilter = new InputFilter\InputFilter();
        $fileInput = new InputFilter\FileInput('image-file');
        $fileInput->setRequired(true);
        $fileInput->getValidatorChain()->attachByName('filemimetype', array('mimeType' => 'image/jpeg'));
        $fileInput->getFilterChain()->attachByName('filerenameupload',
            array(
                'target' => './uploads/images/image.jpg',
                'randomize' => true
            )
        );
        $inputFilter->add($fileInput);
        $this->setInputFilter($inputFilter);
    }

}