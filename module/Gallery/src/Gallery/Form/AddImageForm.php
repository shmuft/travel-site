<?php
/**
 * Created by PhpStorm.
 * User: долгов
 * Date: 22.10.2015
 * Time: 11:22
 */

namespace Gallery\Form;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter;


class AddImageForm extends Form{
    public function __construct($name = 'image'){
        parent::__construct($name);
        $this->addElements();
        $this->addInputFilter();

    }

    public function addElements(){
        $this->add(array(
            'name' => 'id',
            'type' => "Hidden",
        ));
        $this->add(array(
            'name' => 'header',
            'type' => 'Text',
            'options' => array('label' => 'Title',),
        ));

        $file = new Element\File('image-file');
        $file->setLabel('Album image upload')->setAttribute('imageFile', 'image-file');
        $this->add($file);

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Upload',
                'id' => 'submittbutton',
            ),
        ));
    }

    public function addInputFilter(){
        $inputFilter = new InputFilter\InputFilter();
        $fileInput = new InputFilter\FileInput('image-file');
        $fileInput->setRequired(true);
        $fileInput->getValidatorChain()->attachByName('filemimetype', array('mimeType' => 'image/jpeg'));
        $fileInput->getFilterChain()->attachByName('filerenameupload',
            array(
                'target' => './uploads/images/image.jpg',
                'randomize' => true
            )
        );
        $inputFilter->add($fileInput);
        $this->setInputFilter($inputFilter);
    }

}