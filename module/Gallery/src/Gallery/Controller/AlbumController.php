<?php

namespace Gallery\Controller;

use Doctrine\ORM\EntityManager,
    Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Zend\Session\Container,
    Gallery\Entity\Album;
use Gallery\Form\AddAlbumForm as AddAlbumForm;
use Gallery\Form\EditAlbumForm as EditAlbumForm;
use DoctrineORMModule\Proxy\__CG__\Gallery\Entity\Gallery;


/**
 * Controller of gallery.
 *
 * @author Ivan Dolgov <shmuft@yandex.ru>
 */
class AlbumController extends AbstractActionController
{
    /**
     * @var \Zend\Session\Container
     */
    protected $userSession;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Set the entity manager.
     *
     * EntityManager is set on bootstrap.
     *
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Get the entity manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * Return the user session container.
     *
     * @return \Zend\Session\Container
     */
    public function getUserSession()
    {
        if ($this->userSession === null) {
            $this->userSession = new Container('user');
        }

        return $this->userSession;
    }

    /**
     * Display a gallery
     *
     * @return array
     */
//    public function showGalleryByIdUserAction()
//    {
//        $em = $this->getEntityManager();
//        $username = $this->getEvent()->getRouteMatch()->getParam('username');
//        if ($username === null) {
//            return $this->redirect()->toRoute('gallery');
//        }
//
//        $owner = $this->em->getRepository('User\Entity\User')->findOneByUsername($username);
//        if ($owner === null) {
//            return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
//        }
//    }

    /**
     * Display an album
     *
     * @return array
     */
    public function showAction()
    {
        $em = $this->getEntityManager();

        $owner = $this->em->getRepository('User\Entity\User')->findOneByUsername('shmuft');

        if ($owner === null) {
            return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
        }

        $user = null;
        if ($this->getUserSession()->offsetExists('user')) {
            $user = $this->getEntityManager()->getRepository('User\Entity\User')
                ->find($this->getUserSession()->offsetGet('user')->getId());
            $albums = $em->getRepository('Gallery\Entity\Album')->findBy(
                array('gallery_id' => $user->getGallery())
            );

            if (empty($images)) {
                $message = sprintf('Your gallery is empty. You can add some images to your gallery by following link on the top page!');
                $this->flashMessenger()->setNamespace('info')->addMessage($message);
            }
        } else {
            $images = $this->getEntityManager()->getRepository('Gallery\Entity\Image')->getImages($owner->getGallery());
        }

        $view = new ViewModel();
        $view
            ->setVariable('albums', $albums)
            ->setVariable('owner', $owner)
            ->setVariable('user', $user);

        return $view;
    }

    public function addAction(){

        $owner = $this->em->getRepository('User\Entity\User')->findOneByUsername('shmuft');

        if ($owner === null) {
            return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
        }

        $form = new AddAlbumForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $form->setData($data);
            //$form->setInputFilter(new AddTravelFormValidator());

            if ($form->isValid()) {

                $new_album = new Album();
                $new_album->setGalleryId($owner->getGallery());
                $new_album->setHeader($data['header']);
                $this->getEntityManager()->persist($new_album);
                $this->getEntityManager()->flush();

                $message = sprintf('new album test was succesfully added to albums!');
                $this->flashMessenger()->setNamespace('success')->addMessage($message);

                echo "GUT!!!";
                return $this->redirect()->toUrl('/');
            }
        }
        return array(
            'form' => $form,
        );

//
    }

    public function editAction(){

        $owner = $this->em->getRepository('User\Entity\User')->findOneByUsername('shmuft');

        if ($owner === null) {
            return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
        }

        $form = new EditAlbumForm();

        $request = $this->getRequest();
        if (!$request->isPost()){
            $id =  (int) $this->params()->fromRoute('id', 0);
            if (!$id) {
                $this->flashMessenger()->addErrorMessage('album id doesn\'t set');
                return $this->redirect()->toRoute('album');
            }
            $edit_album = $this->em->getRepository('Gallery\Entity\Album')->findOneBy(array('id'=>$id));
            $form->setData(array(
                'id' => $edit_album->getId(),
                'header' => $edit_album->getHeader(),
            ));
        }

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $form->setData($data);

            if ($form->isValid()) {

                $edit_album = $this->em->getRepository('Gallery\Entity\Album');
                $edit_album->setHeader($data['header']);

                $new_image = new Image();
                $new_image->setHeader('AlbumTitle');
                $new_image->setFile($data['image-file']['tmp_name']);

                $this->getEntityManager()->persist($new_image);
                $edit_album->setImage($new_image);
                $this->getEntityManager()->persist($edit_album);
                $this->getEntityManager()->flush();

                $message = sprintf('new album test was succesfully added to albums!');
                $this->flashMessenger()->setNamespace('success')->addMessage($message);

                echo "GUT!!!";
                return $this->redirect()->toUrl('/');
            }
        }
        return array(
            'form' => $form,
        );

//
    }

    /**
     * Show all albums
     *
     * @return array|\Zend\View\Helper\ViewModel
     */
    public function indexAction()
    {
        $allAlbums = $this->getEntityManager()->getRepository('Gallery\Entity\Album')->getAllPublicAlbums();

        // If there is no gallery in the application, we display a message to user.
        if (empty($allAlbums)) {
            $view = new ViewModel();
            $view->setTemplate('gallery/gallery/empty');

            return $view;
        }


        //$randomGallery = $allGallery[rand(0, count($allGallery) - 1)];
//        $images = $this->getEntityManager()->getRepository('Gallery\Entity\Image')->getImages($randomGallery);

        return array(
            'allAlbums'    => $allAlbums,
//            'images'        => $images,
        );
    }
}
