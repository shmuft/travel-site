<?php
/**
 * Created by PhpStorm.
 * User: Долгов
 * Date: 21.01.15
 * Time: 13:32
 */

namespace Gallery\Controller;
use Doctrine\ORM\EntityManager,
    Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Zend\Session\Container,
    Gallery\Entity\Image,
    Gallery\Form\AddImageForm;

class ImageController extends AbstractActionController{

    /**
     * @var \Zend\Session\Container
     */
    protected $userSession;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Set the entity manager.
     *
     * EntityManager is set on bootstrap.
     *
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Get the entity manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * Return the user session container.
     *
     * @return \Zend\Session\Container
     */
    public function getUserSession()
    {
        if ($this->userSession === null) {
            $this->userSession = new Container('user');
        }

        return $this->userSession;
    }
    public function addAction(){

        $tempFile = null;
        $owner = $this->em->getRepository('User\Entity\User')->findOneByUsername('shmuft');

        if ($owner === null) {
            return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
        }

        $form = new AddImageForm();
        $prg = $this->fileprg($form);

        if ($prg instanceof \Zend\Http\PhpEnvironment\Response) {
            return $prg;
        }
        elseif (is_array($prg)){
            if ($form->isValid()) {

                $data = $form->getData();

                $new_image = new Image();
                $new_image->setHeader($data['header']);
                $new_image->setFile($data['image-file']['tmp_name']);

                $this->getEntityManager()->persist($new_image);
                $this->getEntityManager()->flush();

                $message = sprintf('new image was succesfully added to albums!');
                $this->flashMessenger()->setNamespace('success')->addMessage($message);
//
//                echo "GUT!!!";
                return $this->redirect()->toUrl('/');
            }
        }

//        $request = $this->getRequest();
//        if ($request->isPost()) {
//            $post = array_merge_recursive(
//                $request->getPost()->toArray(),
//                $request->getFiles()->toArray()
//            );
////            $data = $request->getPost()->toArray();
//            $form->setData($post);
//            //$form->setInputFilter(new AddTravelFormValidator());
//
//
//        }
        return array(
            'form' => $form,
            'tempFile' => $tempFile,
        );

//
    }
}