<?php
/**
 * Created by PhpStorm.
 * User: Иванус
 * Date: 14.01.15
 * Time: 23:47
 */

namespace Gallery\Repository;
use Doctrine\ORM\EntityRepository;

/**
 * Repository of album object.
 *
 * @author Ivan Dolgov <shmuft@yandex.ru>
 */
class AlbumRepository extends EntityRepository{

}
