<?php

namespace Gallery\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository of gallery object.
 *
 * @author Ivan Dolgov <shmuft@yandex.ru>
 */
class GalleryRepository extends EntityRepository
{
    /**
     * Return all public gallery.
     *
     * @return array
     */
    public function getAllPublicGallery()
    {
        $qb = $this->createQueryBuilder('gallery');
        $qb
            ->addSelect('gallery, user')
            ->innerJoin('gallery.owner', 'user');
            //->leftJoin('gallery.images', 'image')
            //->andWhere('image.order IS NOT NULL')
            //->orderBy('image.order', 'ASC');

        return $qb->getQuery()->getResult();
    }
}
