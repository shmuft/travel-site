<?php

namespace Gallery\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\Common\Collections\ArrayCollection;

use User\Entity\User;


/**
 * Represent a user gallery.
 *
 * @ORM\Entity(repositoryClass = "Gallery\Repository\GalleryRepository")
 * @ORM\Table(name = "gallery")
 *
 * @author Ivan Dolgov <shmuft@yandex.ru>
 */
class Gallery
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer");
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User The owner of the gallery.
     *
     * @ORM\OneToOne(targetEntity = "\User\Entity\User")
     * @ORM\JoinColumn(
     *      name                 = "owner_id",
     *      referencedColumnName = "user_id"
     * )
     */
    private $owner;

    /**
     * @var array The albums.
     *
     * @ORM\OneToMany(
     *      targetEntity = "Album",
     *      mappedBy     = "gallery_id",
     *      cascade      = { "all" }
     * )
     */
    private $albums;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->albums = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get gallery owner.
     *
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set order.
     *
     * @param \User\Entity\User $owner
     *
     * @return Gallery
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;

//        return $this;
    }

    /**
     * Get albums.
     *
     * @return array
     */
    public function getAlbums()
    {
        return $this->albums;
    }

    /**
     * Set album.
     *
     * @param array|ArrayCollection $albums
     *
     * @return Gallery
     */
    public function setAlbums($albums)
    {
        foreach ($albums as $album) {
            $this->addAlbum($album);
        }

        return $this;
    }

    /**
     * Add album to the gallery by owner.
     *
     * @param Album $album The album.
     *
     * @return Gallery
     */
    public function addAlbum(Album $album)
    {
        $this->albums[] = $album;
        return $this;
    }
}
