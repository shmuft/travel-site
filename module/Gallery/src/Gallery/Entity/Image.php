<?php
namespace Gallery\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\Common\Collections\ArrayCollection;

/**
 * Created by PhpStorm.
 * User: Иванус
 * Date: 09.10.14
 * Time: 22:07
 */

/**
 * Represent a user image.
 *
 * @ORM\Entity(repositoryClass = "Gallery\Repository\ImageRepository")
 * @ORM\Table(name = "image")
 *
 * @author Ivan Dolgov <shmuft@yandex.ru>
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer");
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Album",
     *      inversedBy   = "images"
     * );
     * @ORM\JoinColumn(
     *      name="album_id",
     *      referencedColumnName="id"
     * );
     */

    private $album_id;

    /**
     * @var string header.
     *
     * @ORM\Column(
     *      type   = "string",
     *      length = 255
     * )
     */
    private $header;

    /**
     * @var string file.
     *
     * @ORM\Column(
     *      type   = "string",
     *      length = 255
     * )
     */
    private $file;



    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

//    /**
//     * Get images.
//     *
//     * @return array
//     */
//    public function getImages()
//    {
//        return $this->images;
//    }
//
//    /**
//     * Set images.
//     *
//     * @param array|ArrayCollection $images
//     */
//    public function setImages($images)
//    {
//        foreach ($images as $image) {
//            $this->addImage($image);
//        }
//
//        //return $this;
//    }

    /**
     * Add image to the album by owner.
     *
     * @param \Gallery\Entity\Album|\Gallery\Entity\Image $image
     */
//    public function addImage(Album $image)
//    {
//        $this->images[] = $image;
//        //return $this;
//    }

    public function setHeader($header){
        $this->header = $header;
    }

    public function getHeader(){
        return $this->header;
    }

    public function setFile($file){
        $this->file = $file;
    }

    public function getFile(){
        return $this->file;
    }
}
