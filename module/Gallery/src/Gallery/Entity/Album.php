<?php
namespace Gallery\Entity;

use Doctrine\ORM\Mapping as ORM,
    Doctrine\Common\Collections\ArrayCollection;

/**
 * Represent a user gallery.
 *
 * @ORM\Entity(repositoryClass = "Gallery\Repository\AlbumRepository")
 * @ORM\Table(name = "album")
 *
 * @author Ivan Dolgov <shmuft@yandex.ru>
 */
class Album
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer");
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var array The images.
     *
     * @ORM\OneToMany(
     *      targetEntity = "Image",
     *      mappedBy     = "album_id",
     *      cascade      = { "all" }
     * );
     */
    private $images;

    /**
     * @ORM\ManyToOne(
     *      targetEntity = "Gallery",
     *      inversedBy   = "albums"
     * );
     * @ORM\JoinColumn(
     *      name="gallery_id",
     *      referencedColumnName="id"
     * );
     */

    private $gallery_id;

    /**
     * @var string header.
     *
     * @ORM\Column(
     *      type   = "string",
     *      length = 255
     * )
     */
    protected $header;

    /**
     * @var Image.
     *
     * @ORM\OneToOne(
     *      targetEntity = "Image"
     * );
     */
    protected $albumImage;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get gallery_id
     *
     * @return integer
     */

    public function getGalleryId(){
        return $this->gallery_id;
    }

    /**
     * Set gallery_id
     */

    public function setGalleryId($gallery_id){
        $this->gallery_id = $gallery_id;
    }

    /**
     * Get header
     *
     * @return integer
     */
    public function getHeader(){
        return $this->header;
    }

    /**
     * Set header
     */
    public function setHeader($header){
        $this->header = $header;
    }

    /**
     * Get Image
     *
     * @return Image
     */
    public function getAlbumImage(){
        return $this->albumImage;
    }

    /**
     * Set header
     */
    public function setAlbumImage($albumImage){
        $this->albumImage = $albumImage;
    }

    /**
     * Get images.
     *
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set images.
     *
     * @param array|ArrayCollection $images
     *
     * @return image
     */
    public function setImages($images)
    {
        foreach ($images as $image) {
            $this->addImage($image);
        }

        return $this;
    }

    /**
     * Add image to the album by owner.
     *
     * @param Image $image
     */
    public function addImage(Image $image)
    {
        $this->images[] = $image;
  //      return $this;
    }
}
