<?php
/**
 * Created by PhpStorm.
 * User: Samsung
 * Date: 13.07.14
 * Time: 22:20
 */
namespace User\Controller;

use Gallery\Entity\Gallery;
use WhereTravelAndSleep\Entity\WhereTravel;
use Zend\Mvc\Controller\AbstractActionController,
    Zend\Session\Container,
    Doctrine\ORM\EntityManager;


class IndexController extends AbstractActionController {
    /**
     * @var \Zend\Session\Container
     */
    protected $userSession;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Set the entity manager.
     *
     * EntityManager is set on bootstrap.
     *
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Get the entity manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * Return the user session container.
     *
     * @return Zend\Session\Container
     */
    public function getUserSession()
    {
        if ($this->userSession === null) {
            $this->userSession = new Container('user');
        }

        return $this->userSession;
    }

    public function indexAction()
    {
        $id = $this->params()->fromPost('id');

        $entityManager = $this->getEntityManager();

        $user = $this->getUserSession()->offsetGet('user');

        if ($user === null) {
            return;
        }

        $user = $this->getEntityManager()->getRepository('User\Entity\User')->find($user->getId());


    }

    public function addIndexAction()
    {
        $id = $this->params()->fromPost('id');

        $entityManager = $this->getEntityManager();

        $user = $this->getUserSession()->offsetGet('user');

        if ($user === null) {
            return;
        }

        $user = $this->getEntityManager()->getRepository('User\Entity\User')->find($user->getId());
        $travel = new WhereTravel();
        $travel->setCoordinates("12 42");
        $travel->setDescription("123");
        $travel->setHeader("asdasd");
        $travel->setAlbumId("1");
        $travel->setUserId($user);
        $this->getEntityManager()->persist($travel);

        $user->addWhereTravel($travel);


        $this->getEntityManager()->flush();
    }

}