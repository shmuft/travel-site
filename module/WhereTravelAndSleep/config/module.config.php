<?php

namespace WhereTravelAndSleep;

return array(
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            ),
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'where-travel' => __DIR__ . '/../view',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'index'         => 'WhereTravelAndSleep\Controller\IndexController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'index' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/travel/index',
                    'defaults' => array(
                        'controller' => 'index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'add-travel' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/travel/add-travel',
                    'defaults' => array(
                        'controller' => 'index',
                        'action'     => 'addTravel',
                    ),
                ),
            ),
            'json-get-travels' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/travel/json-get-travels',
                    'defaults' => array(
                        'controller' => 'index',
                        'action'     => 'jsonGetTravels',
                    ),
                ),
            ),
        ),
    ),



//    'zfcuser' => array(
//        // telling ZfcUser to use our own class
//        'user_entity_class'       => 'MyUser\Entity\User',
//        // telling ZfcUserDoctrineORM to skip the entities it defines
//        'enable_default_entities' => false,
//    ),
//
//    'bjyauthorize' => array(
//        // Using the authentication identity provider, which basically reads the roles from the auth service's identity
//        'identity_provider' => 'BjyAuthorize\Provider\Identity\AuthenticationIdentityProvider',
//
//        'role_providers'        => array(
//            // using an object repository (entity repository) to load all roles into our ACL
//            'BjyAuthorize\Provider\Role\ObjectRepositoryProvider' => array(
//                'object_manager'    => 'doctrine.entity_manager.orm_default',
//                'role_entity_class' => 'MyUser\Entity\Role',
//            ),
//        ),
//    ),
);
