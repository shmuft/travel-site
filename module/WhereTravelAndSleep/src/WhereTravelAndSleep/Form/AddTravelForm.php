<?php
namespace WhereTravelAndSleep\Form;

use Zend\Form\Form,
    Zend\Form\Element,
    Zend\Captcha;

/**
 * Represent a register form.
 */
class AddTravelForm extends Form
{
    /**
     * {@inheritdoc}
     */
    public function __construct($name = null)
    {
        parent::__construct();
        $album_id = new Element\Text('album_id');
        $header = new Element\Text('header');
        $description2 = new Element\Textarea('description');
        $description2->setLabel('DESCRIPTION!');
        $anons = new Element\Textarea('anons');
        $coordinates = new Element\Text('coordinates');


//        $figlet = new Captcha\Figlet();
//        $figlet->setWordLen(5);
//        $captcha = new Element\Captcha('captcha');
//        $captcha
//            ->setCaptcha($figlet)
//            ->setAttribute('placeholder', 'Enter the captcha here');

        $this->setAttribute('method', 'post');

        $this->add($album_id);
        $this->add($header);
        $this->add($anons);
        $this->add($description2);
        $this->add($coordinates);
//        $this->add($captcha);
    }
}
