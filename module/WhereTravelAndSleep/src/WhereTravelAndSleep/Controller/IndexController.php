<?php
/**
 * Created by PhpStorm.
 * User: Долгов
 * Date: 13.08.14
 * Time: 11:55
 */

namespace WhereTravelAndSleep\Controller;

use Gallery\Entity\Gallery;
use WhereTravelAndSleep\Form\AddTravelForm;
use WhereTravelAndSleep\Form\AddTravelFormValidator;
use Zend\View\Model\ViewModel;
use WhereTravelAndSleep\Entity\WhereTravel;
use Zend\Mvc\Controller\AbstractActionController,
    Zend\Session\Container,
    Doctrine\ORM\EntityManager;
use Zend\Json\Json as JsonMy;



class IndexController extends AbstractActionController {
    /**
     * @var \Zend\Session\Container
     */
    protected $userSession;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * Set the entity manager.
     *
     * EntityManager is set on bootstrap.
     *
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Get the entity manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * Return the user session container.
     *
     * @return Zend\Session\Container
     */
    public function getUserSession()
    {
        if ($this->userSession === null) {
            $this->userSession = new Container('user');
        }

        return $this->userSession;
    }

    public function indexAction()
    {
        $id = $this->params()->fromPost('id');

        $entityManager = $this->getEntityManager();

        $user = $this->getUserSession()->offsetGet('user');

//        if ($user === null) {
//            return;
//        }

       // $user = $this->getEntityManager()->getRepository('User\Entity\User')->find($user->getId());

       // $list = $this->getEntityManager()->getRepository('WhereTravelAndSleep\Entity\WhereTravel')->findAll();

        $view = new ViewModel();
        $view
            ->setTemplate('where-travel-and-sleep/travel/index');
         //   ->setVariable('list', $list);

        return $view;

    }

    public function jsonGetTravelsAction(){
        $request = $this->getRequest();
        $travels = $this->getEntityManager()->getRepository('WhereTravelAndSleep\Entity\WhereTravel')->findAll();
        $jsonN = array();
        foreach ($travels as $travel){
            $coord = explode(',',$travel->getCoordinates());
            $item = array(
                'id' => $travel->getId(),
                'header' => $travel->getHeader(),
                'anons' => $travel->getAnons(),
                'description' => $travel->getDescription(),
                'coordinate1' => $coord[0],
                'coordinate2' => $coord[1]
            );
            $jsonN[] = $item;

        }

        $jsonNative = JsonMy::encode($jsonN);
//            $jsonNative = JsonMy::prettyPrint($jsonNative);

        $view = new ViewModel();
        $view->setTemplate('where-travel-and-sleep/travel/json')
             ->setVariable('printOut', $jsonNative);
        header ( 'Content-Type: application/json' );
        $view->setTerminal(true);
        return $view;
    }


    /**
     * Add new travel.
     *
     * @return array
     */

    public function addTravelAction()
    {
        $viewmodel = new ViewModel();
        $viewmodel->setTemplate('where-travel-and-sleep/travel/add-travel');
        $viewmodel->setTerminal(true);
        $user = $this->getUserSession()->offsetGet('user');
        if ($user === null) {
            return;
        }
        $owner = $this->getEntityManager()->getRepository('User\Entity\User')->find($user->getId());

        $form = new AddTravelForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $travel = new WhereTravel();

            $data = $request->getPost()->toArray();
//            $filePost = $this->params()->fromFiles('image');
//            $file = array('image' => $filePost['tmp_name']);
//            $data    = array_merge_recursive(
//                $this->getRequest()->getPost()->toArray(),
//                $this->getRequest()->getFiles()->toArray()
//            );
            //$data = array_merge($data, $file, $filePost);

            $form->setData($data);
            $form->setInputFilter(new AddTravelFormValidator());

            if ($form->isValid()) {

                  echo "GUT!!!";
//                list($width, $height, $type, $attr) = getimagesize($filePost['tmp_name']);
//
                $travel->populate($form->getData(),$owner);
//                $image
//                    ->setFile($filePost)
//                    ->setName($filePost['name'])
//                    ->setWidth($width)
//                    ->setHeight($height)
//                    ->setGallery($owner->getGallery());
//
//                $owner->getGallery()->addImage($image);
                $this->getEntityManager()->persist($travel);
                $this->getEntityManager()->flush();
//
                $message = sprintf('%s was succesfully added to travels!', $travel->getHeader());
                $this->flashMessenger()->setNamespace('success')->addMessage($message);
//
                return $this->redirect()->toUrl($_SERVER['HTTP_REFERER']);
            }
        }

        $viewmodel->setVariable('form', $form);

        return $viewmodel;
    }

//    public function addTravelAction()
//    {
//        $id = $this->params()->fromPost('id');
//
//        $entityManager = $this->getEntityManager();
//
//        $user = $this->getUserSession()->offsetGet('user');
//
//        if ($user === null) {
//            return;
//        }
//
//
//        $user = $this->getEntityManager()->getRepository('User\Entity\User')->find($user->getId());
//        $travel = new WhereTravel();
//        $travel->setCoordinates("12 42");
//        $travel->setDescription("123");
//        $travel->setHeader("asdasd");
//        $travel->setAlbumId("1");
//        $travel->setUserId($user);
//        $this->getEntityManager()->persist($travel);
//
//        $user->addWhereTravel($travel);
//
//
//        $this->getEntityManager()->flush();
//    }

}