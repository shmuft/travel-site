<?php
/**
 * Created by PhpStorm.
 * User: Samsung
 * Date: 05.06.14
 * Time: 20:58
 */

namespace WhereTravelAndSleep\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/** @ORM\Entity
 * @ORM\Table(name="WhereTravel")
 */

class WhereTravel {

    /**
    * @var int
    * @ORM\Id
    * @ORM\Column(type="integer");
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
     * @var int
     * @ORM\Column(type="integer");
     */

    protected $album_id;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    protected $header;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    protected $description;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    protected $anons;
    /**
     * @var User the owner of travel place
     *
     * @ORM\ManyToOne(
     *      targetEntity    = "\User\Entity\User",
     *      inversedBy="whereTravels"
     * )
     * @ORM\JoinColumn(name = "user_id",
     *  referencedColumnName= "user_id")
     *
     */
    protected $user_id;

    /**
     * @var string
     * @ORM\Column(type="string", length=40, nullable=false)
     */
    protected $coordinates;

    public function getId(){
        return $this->id;
    }

    public function getAlbumId(){
        return $this->album_id;
    }

    public function setAlbumId($album){
        $this->album_id = $album;
    }

    public function getHeader(){
        return $this->header;
    }

    public function setHeader($header){
        $this->header = $header;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        $this->description = $description;
    }

    public function getAnons(){
        return $this->anons;
    }

    public function setAnons($anons){
        $this->anons = $anons;
    }

    public function getCoordinates(){
        return $this->coordinates;
    }

    public function setCoordinates($coordinates){
        $this->coordinates = $coordinates;
    }

    public function getUserId(){
        return $this->user_id;
    }

    public function setUserId($userId){
        $this->user_id = $userId;
    }

    /**
     * This method is use by controller for bind form.
     *
     * @param array $data
     */
    public function populate($data,$user_id)
    {
        $this->setAlbumId($data['album_id']);
        $this->setCoordinates($data['coordinates']);
        $this->setDescription($data['description']);
        $this->setHeader($data['header']);
        $this->setUserId($user_id);
        $this->setAnons($data['anons']);
    }


}