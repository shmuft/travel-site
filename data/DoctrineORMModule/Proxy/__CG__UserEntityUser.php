<?php

namespace DoctrineORMModule\Proxy\__CG__\User\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class User extends \User\Entity\User implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array();



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return array('__isInitialized__', 'id', 'username', 'email', 'password', 'plainPassword', 'age', 'gallery', 'whereTravels', 'enable', 'attempt', 'admin');
        }

        return array('__isInitialized__', 'id', 'username', 'email', 'password', 'plainPassword', 'age', 'gallery', 'whereTravels', 'enable', 'attempt', 'admin');
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (User $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', array());
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', array());
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getWhereTravels()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getWhereTravels', array());

        return parent::getWhereTravels();
    }

    /**
     * {@inheritDoc}
     */
    public function setWhereTravels($whereTravels)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setWhereTravels', array($whereTravels));

        return parent::setWhereTravels($whereTravels);
    }

    /**
     * {@inheritDoc}
     */
    public function addWhereTravel($addTtavel)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addWhereTravel', array($addTtavel));

        return parent::addWhereTravel($addTtavel);
    }

    /**
     * {@inheritDoc}
     */
    public function deleteWhereTravel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'deleteWhereTravel', array());

        return parent::deleteWhereTravel();
    }

    /**
     * {@inheritDoc}
     */
    public function editWhereTravel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'editWhereTravel', array());

        return parent::editWhereTravel();
    }

    /**
     * {@inheritDoc}
     */
    public function getAge()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAge', array());

        return parent::getAge();
    }

    /**
     * {@inheritDoc}
     */
    public function setAge($age)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAge', array($age));

        return parent::setAge($age);
    }

    /**
     * {@inheritDoc}
     */
    public function getGallery()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getGallery', array());

        return parent::getGallery();
    }

    /**
     * {@inheritDoc}
     */
    public function setGallery(\Gallery\Entity\Gallery $gallery)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setGallery', array($gallery));

        return parent::setGallery($gallery);
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', array());

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEmail', array());

        return parent::getEmail();
    }

    /**
     * {@inheritDoc}
     */
    public function setEmail($email)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEmail', array($email));

        return parent::setEmail($email);
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUsername', array());

        return parent::getUsername();
    }

    /**
     * {@inheritDoc}
     */
    public function setUsername($username)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUsername', array($username));

        return parent::setUsername($username);
    }

    /**
     * {@inheritDoc}
     */
    public function getPassword()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPassword', array());

        return parent::getPassword();
    }

    /**
     * {@inheritDoc}
     */
    public function setPassword($password)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPassword', array($password));

        return parent::setPassword($password);
    }

    /**
     * {@inheritDoc}
     */
    public function getPlainPassword()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPlainPassword', array());

        return parent::getPlainPassword();
    }

    /**
     * {@inheritDoc}
     */
    public function setPlainPassword($password)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPlainPassword', array($password));

        return parent::setPlainPassword($password);
    }

    /**
     * {@inheritDoc}
     */
    public function getAttempt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAttempt', array());

        return parent::getAttempt();
    }

    /**
     * {@inheritDoc}
     */
    public function addAttempt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addAttempt', array());

        return parent::addAttempt();
    }

    /**
     * {@inheritDoc}
     */
    public function resetAttempt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'resetAttempt', array());

        return parent::resetAttempt();
    }

    /**
     * {@inheritDoc}
     */
    public function isEnable()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isEnable', array());

        return parent::isEnable();
    }

    /**
     * {@inheritDoc}
     */
    public function setEnable($enable)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEnable', array($enable));

        return parent::setEnable($enable);
    }

    /**
     * {@inheritDoc}
     */
    public function isAdmin()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isAdmin', array());

        return parent::isAdmin();
    }

    /**
     * {@inheritDoc}
     */
    public function generatePassword($lenght = 6)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'generatePassword', array($lenght));

        return parent::generatePassword($lenght);
    }

    /**
     * {@inheritDoc}
     */
    public function populate(array $data)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'populate', array($data));

        return parent::populate($data);
    }

    /**
     * {@inheritDoc}
     */
    public function encryptPassword()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'encryptPassword', array());

        return parent::encryptPassword();
    }

}
